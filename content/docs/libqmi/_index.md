---
title: "libqmi"
linkTitle: "libqmi"
weight: 3
description: >
  The GLib-based libqmi library to use the QMI protocol
---

This section provides information about the `libqmi` library.
