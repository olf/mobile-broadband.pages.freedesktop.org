---
title: "ModemManager"
linkTitle: "ModemManager"
weight: 4
description: >
  The ModemManager project
---

This section provides information about the `ModemManager` daemon, the `libmm-glib` library, and the `mmcli` command line tool.
