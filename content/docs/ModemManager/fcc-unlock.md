---
title: "FCC unlock procedure"
linkTitle: "FCC unlock"
weight: 3
description: >
  FCC unlock procedure integration in ModemManager.
---

## What is the FCC lock?

The FCC lock is a software lock integrated in WWAN modules shipped by several different laptop manufacturers like Lenovo, Dell, or HP. This locks prevents the WWAN module from being put online until some specific unlock procedure (usually a magic command sent to the module) is done.

If the specific unlock procedure is not done, any attempt to enable the radio with any kind of protocol will fail:
 * `AT+CFUN=1` will fail, likely with just an `ERROR` response.
 * `QMI DMS Set Operating Mode (ONLINE)` will fail, likely with an `Invalid transition` error.
 * `MBIM Radio State (ON)` will fail, likely with an `Operation not allowed` error.

The purpose of this lock is, according to the manufacturers, to have a way to bind the WWAN module to a specific laptop, so that the whole bundle of laptop+module can go through the FCC certification process for radio-enabled devices in the USA. This lock has no other known purpose out of the US regulation.

The FCC lock is also part of a mutual authentication attempt between module and laptop:
 * The laptop BIOS comes with an allowlist of modules that can be installed in the laptop.
 * The WWAN module comes with a FCC lock that should be unlocked by approved laptops only.

Laptop manufacturers should in theory provide a custom FCC unlock tool so that users can use the modules, but the reality is sometimes far from ideal:
 * The unlock tools may be provided only for Windows, if the laptops were not FCC certified to be used in GNU/Linux distributions.
 * The unlock tools are usually provided as proprietary binary programs that are non-auditable and still need to be run as root in the system.
 * The unlock tools will perform system checks to detect which kind of laptop the module is installed in, and if it is not one of the laptops certified by the vendor for use under GNU/Linux, the tool may not unlock the modem, even if the same setup can be used in Windows.
 * The unlock tools sometimes fail to be available for Linux users by the time the laptops are ready to be sold.

## FCC unlock procedures in ModemManager < 1.18.4

When the first FCC unlocked modems were shipped in Dell laptops sometime around 2015, users of GNU/Linux distributions [started to complain](https://bugzilla.kernel.org/show_bug.cgi?id=92101) about not being able to use their WWAN modules. These modems were rebranded Sierra Wireless devices, and surprisingly, using the Sierra Wireless provided kernel drivers the modules were unlocked successfully. This lead to finding the `QMIDMSSWISetFCCAuth()` operation (DMS `0x555F`) in the Sierra SDK, which was [successfully reverse engineered](https://sigquit.wordpress.com/2015/02/09/dell-branded-sierra-wireless-3g4g-modem-not-online/) into [libqmi 1.12.4](https://lists.freedesktop.org/archives/libqmi-devel/2015-February/001106.html).

The FCC unlock logic done for these modules was added into [ModemManager 1.4.4](https://lists.freedesktop.org/archives/modemmanager-devel/2015-February/001757.html) for all QMI capable devices, just attempting to run it any time we found the attempt to put the module online failed.

In 2018, Dell included newer FCC locked modems, like the Foxconn rebranded DW5821e, in their laptops. Dell also provided a Windows unlock tool (DW5821e and DW5829e Click Cust Kit), and likely some built-in unlock tool hidden in the Ubuntu-based preinstallations they shipped in the laptops when sold with GNU/Linux. During this time, there were some attempts to reverse engineer the unlock procedure using a USB monitoring capture from Windows when running the Cust Kit tool, but none of them seemed to be usable for some reason.

Fast forward to 2021, when Lenovo shipped the newest X1 Nano and Carbon Gen9 laptops with new FCC locked modules, and users [started to complain again](https://lists.freedesktop.org/archives/modemmanager-devel/2021-May/008571.html). These laptops were released with 3 different approved modules: Foxconn SDX55, Quectel EM120, and Fibocom L860. By the time the first SDX55 modules were available there was still no unlock tool provided by Lenovo, so there was not much anyone could do. But the SDX55 was a Foxconn module, same as the DW5821e for which the Dell Cust Kit tool was reverse engineered, and so users tried that same procedure that never worked with the DW5821e, just to find that [it worked fine with the SDX55](https://lists.freedesktop.org/archives/modemmanager-devel/2021-May/008588.html). This unlock procedure was included in [libqmi 1.28.6](https://lists.freedesktop.org/archives/libqmi-devel/2021-June/003608.html), and integrated for automatic unlocking in [ModemManager 1.16.6](https://lists.freedesktop.org/archives/modemmanager-devel/2021-June/008660.html).

Not much later, [users started to complain](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues/402) about the Quectel EM120 module being locked. A user of this module was able reverse engineer the Lenovo-provided `lenovo-wwan-dpr` tool and discover [the magic MBIM command](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues/402#note_1064882) that was being sent in this case. This procedure has not been integrated for automatic unlock in any ModemManager version.

## Whether to automatically unlock or not the modules in ModemManager

When the first FCC unlock procedure was reverse engineered for the rebranded Sierra Wireless modules, there was not much discussion on whether the procedure should be run or not by ModemManager. It was obviously defeating the purpose of the laptop manufacturer, but so was the Sierra Wireless driver source provided by Sierra Wireless themselves. If they were doing it, why should ModemManager not do it?

The picture with the new WWAN modules in Lenovo laptops is completely different, though. The FCC unlock procedure for the EM120 was reverse engineered from the Lenovo-provided `lenovo-wwan-dpr` tool, which performs not only the FCC unlock, but also dynamic power reduction based on sensors in the laptop. This means that the actual magic command used to unlock the module may be somehow related to some SAR level defaults configured in the WWAN module, and as we don't have the full picture of what it does or doesn't, it can be considered a bit risky to perform the automatic unlock unconditionally. The same reasoning would apply for the Foxconn SDX55, for which not even a Lenovo provided tool was used to reverse engineer the unlock procedure. The sad truth is that only the laptop vendors and modem manufacturers know what those commands are really doing, and they may be doing more than FCC unlocking the modules. If the procedures are related in any way to SAR levels and dynamic power reduction, ModemManager should better not automatically unlock these modules.

This discussion went on for several months, both publicly in gitlab issues and mailing list, and privately via emails and calls with the laptop vendors, modem manufacturers and other key members of the community. After much thought, the ModemManager maintainer took a clear way forward that would hopefully meet halfway of both sides: **ModemManager will keep on providing support for the known FCC unlock procedures, but no longer automatically: the user must install and select the FCC unlock procedure needed in the specific laptop being used.**

## FCC unlock procedures in ModemManager >= 1.18.4

Since release 1.18.4, the ModemManager daemon no longer automatically performs the FCC unlock procedure by default. The user must, **under their own responsibility**, enable the automatic FCC unlock as shipped by ModemManager.

ModemManager ships several scripts installed under `${datadir}/ModemManager/fcc-unlock.available.d`, and named as `vid:pid` with either the PCI or USB vendor and product IDs. E.g. in a system where `${datadir}` is `/usr/share`:
 * For the HP 820 G1 (EM7355): `/usr/share/ModemManager/fcc-unlock.available.d/03f0:4e1d`
 * For the Dell DW5570 (MC8805): `/usr/share/ModemManager/fcc-unlock.available.d/413c:81a3`
 * For the Dell DW5808 (MC7355): `/usr/share/ModemManager/fcc-unlock.available.d/413c:81a8`
 * For the Lenovo-shipped EM7455: `/usr/share/ModemManager/fcc-unlock.available.d/1199:9079`
 * For the Foxconn SDX55: `/usr/share/ModemManager/fcc-unlock.available.d/105b:e0ab`
 * For the Quectel EM120: `/usr/share/ModemManager/fcc-unlock.available.d/1eac:1001`

These scripts in `${datadir}` are not automatically used by ModemManager. In order for ModemManager to automatically attempt a specific FCC unlock procedure, the user must create a link to the script from `${datadir}/ModemManager/fcc-unlock.available.d` into the `${sysconfdir}/ModemManager/fcc-unlock.d` directory. E.g. in a system where `${datadir}` is `/usr/share` and `${sysconfdir}` is `/etc`:
```
 $ sudo ln -sft /etc/ModemManager/fcc-unlock.d /usr/share/ModemManager/fcc-unlock.available.d/<vid>:<pid>
```

ModemManager will only use the unlock script required for the specific `vid:pid` the module exposes, so users may also choose to link all the files from `${datadir}/ModemManager/fcc-unlock.available.d` into `${sysconfdir}/ModemManager/fcc-unlock.d`. E.g. in a system where `${datadir}` is `/usr/share` and `${sysconfdir}` is `/etc`:
```
 $ sudo ln -sft /etc/ModemManager/fcc-unlock.d /usr/share/ModemManager/fcc-unlock.available.d/*
```

It is suggested that **GNU/Linux distributions do not ship these FCC unlock scripts enabled** in the `${sysconfdir}/ModemManager/fcc-unlock.d` directory themselves, for the same reasons that ModemManager does not to perform the automatic unlock unconditionally.

The `${sysconfdir}/ModemManager/fcc-unlock.d` path should be exclusively used for links or programs manually installed by the user.

The scripts shipped by ModemManager use either `mbimcli` or `qmicli` to perform the raw FCC unlock procedure. The user should make sure those tools are installed in the system, as they are not direct dependencies of ModemManager in any way. E.g. in Debian/Ubuntu systems, these tools are provided by the `libmbim-utils` and `libqmi-utils` packages.

## Integration with third party FCC unlock tools

Laptop vendors and modem manufacturers may also provide their own FCC unlock tools, e.g. running their own proprietary binary programs to perform the unlock in the safest way they know. These tools may be packaged for each distribution, and should be installed in `${libdir}/ModemManager/fcc-unlock.d`.

The third party FCC unlock tools will allow an easy integration of the procedure with ModemManager, so that the unlock is performed only when required to do so (when attempting to go online) and not at random times (which would make the state of the modem cached by ModemManager inconsistent).

Third party FCC unlock scripts or programs should consider the same limitations as imposed to the ModemManager shipped ones:
 * Must be owned by the root user.
 * Must be readable and executable by the owner exclusively and not setuid.
 * Will receive the full modem DBus path as first argument, followed by the control port names (without `/dev` prefix) of the modem to unlock.
 * Must not launch processes on background; i.e. these are not service management scripts.
 * Must run in less than 5 seconds, or otherwise they will be killed by ModemManager.
 * Must return 0 on success.

Once the tool is run by ModemManager, the daemon will attempt again to bring the modem into online power state using the protocol specific messages. If this operation fails again, the FCC unlock procedure will not be automatically tried any more.

The `${libdir}/ModemManager/fcc-unlock.d` path should be exclusively used for links or programs installed by third party packages.
