---
title: "libqrtr-glib"
linkTitle: "libqrtr-glib"
weight: 2
description: >
  The GLib-based libqrtr-glib library to use the QRTR protocol
---

This section provides information about the `libqrtr-glib` library.
