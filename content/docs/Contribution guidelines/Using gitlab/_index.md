---
title: "Using gitlab"
linkTitle: "Using gitlab"
weight: 1
description: >
  Steps to setup and use a freedesktop.org gitlab account
---

The [**Mobile Broadband**](https://gitlab.freedesktop.org/mobile-broadband) group in the [gitlab](https://about.gitlab.com) instance maintained by the [freedesktop.org](https://www.freedesktop.org/) community contains the following projects:

 - ModemManager: https://gitlab.freedesktop.org/mobile-broadband/ModemManager
 - libqmi: https://gitlab.freedesktop.org/mobile-broadband/libqmi
 - libmbim: https://gitlab.freedesktop.org/mobile-broadband/libmbim
 - libqrtr-glib: https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib

The following sections provide a quick overview of what the basic functionality is and how to set it up by users or first-time developers. This Community Edition gitlab instance is no different to others, so users that have used gitlab in the past should see no problem in using this site. For detailed help on all the features supported, the gitlab instance also [publishes its own help]( https://gitlab.freedesktop.org/help).