---
title: "Frequently Asked Questions"
linkTitle: "FAQ"
weight: 20
description: >
  Frequently asked questions and general comments.
---

This list provides comments and answers to common questions asked in the projects.

## Can the projects be installed in `/usr/local` instead of `/usr`?

Yes, but several things should be considered.

### Library lookup with `LD_LIBRARY_PATH`

All the build examples given for all the projects in this documentation use the `--prefix=/usr` switch given to the `meson setup` command (or to `./configure` when using GNU autotools) when preparing the build. This prefix is the common one used by distributions when building packages, and so if the user uses the same one, it will end up overwriting the package manager installed files.

In order to avoid overwriting the package manager installed files, the user can avoid giving the explicit `--prefix`, and so the default `/usr/local` path will be used as prefix. If this approach is taken, the developer can keep the package manager installed libraries in `/usr/lib` and the package manager installed command line tools in `/usr/bin`, while keeping the custom installed libraries in `/usr/local/lib` and the custom installed command line tools in `/usr/local/bin`.

Under this scenario, the user can run e.g. the package manager installed `mbimcli` in the following way, which will use the package manager installed library from `/usr/lib`:
```
  $ /usr/bin/mbimcli
```

In the same scenario, the user can run the custom installed `mbimcli` in the following way, which will use the custom installed library from `/usr/local/lib`:
```
  $ LD_LIBRARY_PATH=/usr/local/lib /usr/local/bin/mbimcli
```

But **beware** because there are distributions like Debian/Ubuntu which add the `/usr/local/bin` path to the `PATH` environment variable by default, so running the following command would attempt to run the custom installed `mbimcli` with the package manager installed library from `/usr/lib` (which is obviously a mess and won't probably work):
```
  $ mbimcli
```

This complication with the need of the explicit `LD_LIBRARY_PATH` is the reason why this guide suggests to overwrite the package manager installed files when installing custom builds.

### pkg-config lookup with `PKG_CONFIG_PATH`

Building a project using as build dependency another project which was installed in `/usr/local` will need an explicit `PKG_CONFIG_PATH` specifying where the `.pc` files are available, e.g.:
```
  $ PKG_CONFIG_PATH=/usr/local/lib/pkgconfig meson setup ...
```

### Additional system files

If the project installs additional system files (e.g. systemd service setup, polkit settings, DBus service files) in `/usr/local` instead of `/usr`, they will be fully ignored. If the changes being installed require updates in these files, it is advised to avoid installing anywhere else than in `/usr`.

## How long does it take for a merge request or issue review to happen?

There is no maintainer paid to support these projects, so new issues and merge requests are reviewed once the current maintainer finds free time.

If you would like your issue or merge request to be prioritized, please consider [contracting the maintainer (if you're a company)](https://aleksander.es) or [sponsoring (if you're an individual)](https://github.com/sponsors/aleksander0m?o=esb).